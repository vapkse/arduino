#ifndef SmoothAnalogInput_h
#define SmoothAnalogInput_h

#include <Arduino.h>

class SmoothAnalogInput {
    public:
        SmoothAnalogInput();
        void attach(int pin, int size);
        void scale(int min, int max);
        int read();
        int raw();
    private:
		int _size;
        int* _samples;
        int _pin;
        int _index;
        int _mapMin;
        int _mapMax;
        int _res;
};
#endif