#ifndef AmpTransfer_h
#define AmpTransfer_h

#include <SerialBuffer.h>

static const uint8_t SERIAL_BUFFER_SIZE = 64;

// Sequences
static const uint8_t SEQUENCE_STANDBY = 0;    // Stand-by
static const uint8_t SEQUENCE_PRESTANDBY = 1; // Pre-stand-by (before passing to stand-by, HT is off, but heather keep on)
static const uint8_t SEQUENCE_DISCHARGE = 2;  // Discharge
static const uint8_t SEQUENCE_HEAT = 3;       // Heat tempo
static const uint8_t SEQUENCE_STARTING = 4;   // Starting High Voltage
static const uint8_t SEQUENCE_REGULATING = 5; // Waiting for reg
static const uint8_t SEQUENCE_FUNCTION = 6;   // Normal Function
static const uint8_t SEQUENCE_FAIL = 7;       // Fail

static const uint8_t NO_ERROR = 0;
static const uint8_t ERROR_DISHARGETOOLONG = 2;      // 2: Discharge too long
static const uint8_t ERROR_CURRENTONHEAT = 3;        // 3: Current during heat time
static const uint8_t ERROR_REGULATINGTOOLONG = 4;    // 4: Regulation too long
static const uint8_t ERROR_REGULATINGMAXREACHED = 5; // 5: Maximun reached during regulation
static const uint8_t ERROR_REGULATINGMINREACHED = 6; // 6: Minimum reached during regulation
static const uint8_t ERROR_FUNCTIONMAXREACHED = 7;   // 7: Maximun reached during normal function
static const uint8_t ERROR_FUNCTIONMINREACHED = 8;   // 8: Minimum reached during normal function
static const uint8_t ERROR_FUNCTIONOUTOFRANGE = 9;   // 9: Time elapsed with current out of range during normal function
static const uint8_t ERROR_STARTINGTOOLONG = 10;     // 10: Starting too long
static const uint8_t ERROR_STARTINGOUTOFRANGE = 11;  // 11: Out of range during starting
static const uint8_t ERROR_TEMPTOOHIGH = 12;         // 12: Temperature maximum reached
static const uint8_t ERROR_PHASE = 13;               // 13: Phase detection error
static const uint8_t ERROR_EXTERNALSTOP = 14;        // 14: External stop
static const uint8_t ERROR_MINCURRENTREACHED = 15;   // 15: Minimun current reached
static const uint8_t ERROR_MAXCURRENTREACHED = 16;   // 16: Maximun current reached
static const uint8_t ERROR_HIGHVOLTAGEDURINGHEAT = 17;

static const uint8_t ERROR_OFFLINE = 50;
static const uint8_t ERROR_TIMEOUT = 51;
static const uint8_t ERROR_UNKNOWNREQUEST = 52;
static const uint8_t ERROR_WRITINGPARAMS = 53;
static const uint8_t ERROR_WRITINGCONTROLS = 54;
static const uint8_t ERROR_SETWORKINGPOINT = 55;
static const uint8_t ERROR_GLOBALREQUESTVALUES = 56;
static const uint8_t ERROR_BUSY = 57;
static const uint8_t ERROR_ALLOCATIONNOTENOUGH = 58;

// Responses
static const uint8_t RESPONSE_DATA = 100;

// REQUEST Command
static const uint8_t REQUEST_RESET = 70;
static const uint8_t REQUEST_STOP = 71;
static const uint8_t REQUEST_WRITEPARAMS = 80;
static const uint8_t REQUEST_RESETPARAMS = 81;
static const uint8_t REQUEST_RESETMODULATION = 90;

// REQUEST datas
static const uint8_t REQUEST_GETDATA = 100;

// REQUEST params
static const uint8_t REQUEST_GETPARAMS = 111;
static const uint8_t REQUEST_SETWORKINGPOINT = 112;
static const uint8_t REQUEST_SETTEMPMEASUREINTERVAL = 114;
static const uint8_t REQUEST_SETDISCHARGEMAXTIME = 115;
static const uint8_t REQUEST_SETHEATMAXTIME = 116;
static const uint8_t REQUEST_SETHIGHVOLTAGEMAXTIME = 117;
static const uint8_t REQUEST_SETREGULATIONMAXTIME = 118;
static const uint8_t REQUEST_SETOUTOFRANGEMAXTIME = 119;
static const uint8_t REQUEST_SETERRORMAXTIME = 120;
static const uint8_t REQUEST_SETREGULATEDMINTIME = 121;
static const uint8_t REQUEST_SETHIGHVOLTAGEERRORMAXTIME = 122;
static const uint8_t REQUEST_SETEMERGENCYSTOPDELAY = 123;
static const uint8_t REQUEST_SETSTARTP = 124;
static const uint8_t REQUEST_SETSTARTMASTERP = REQUEST_SETSTARTP;
static const uint8_t REQUEST_SETSTARTI = 125;
static const uint8_t REQUEST_SETSTARTMASTERI = REQUEST_SETSTARTI;
static const uint8_t REQUEST_SETSTARTSLAVEP = 126;
static const uint8_t REQUEST_SETSTARTSLAVEI = 127;
static const uint8_t REQUEST_SETREGULATIONP = 128;
static const uint8_t REQUEST_SETREGULATIONMASTERP = REQUEST_SETREGULATIONP;
static const uint8_t REQUEST_SETREGULATIONI = 129;
static const uint8_t REQUEST_SETREGULATIONMASTERI = REQUEST_SETREGULATIONI;
static const uint8_t REQUEST_SETREGULATIONSLAVEP = 130;
static const uint8_t REQUEST_SETREGULATIONSLAVEI = 131;
static const uint8_t REQUEST_SETFUNCTIONP = 132;
static const uint8_t REQUEST_SETFUNCTIONMASTERP = REQUEST_SETFUNCTIONP;
static const uint8_t REQUEST_SETFUNCTIONI = 133;
static const uint8_t REQUEST_SETFUNCTIONMASTERI = REQUEST_SETFUNCTIONI;
static const uint8_t REQUEST_SETFUNCTIONSLAVEP = 134;
static const uint8_t REQUEST_SETFUNCTIONSLAVEI = 135;
static const uint8_t REQUEST_SETREGULATIONTRESHOLD = 137;
static const uint8_t REQUEST_SETSTARTINGTRESHOLD = 138;
static const uint8_t REQUEST_SETFUNCTIONTRESHOLD = 139;
static const uint8_t REQUEST_SETMEASUREAVERAGERATIO = 140;
static const uint8_t REQUEST_SETMODULATIONPEAKAVERAGERATIO = 141;
static const uint8_t REQUEST_SETMODULATIONPEAKREDUCTIONFACTOR = 142;
static const uint8_t REQUEST_SETMODULATIONDETECTEDMINTIME = 143;
static const uint8_t REQUEST_SETSTARTWORKINGPOINT = 144;
static const uint8_t REQUEST_SETMINPOINT = 145;
static const uint8_t REQUEST_SETMAXPOINT = 146;
static const uint8_t REQUEST_SETTEMPMAX = 147;
static const uint8_t REQUEST_SETTEMPAIRMAX = REQUEST_SETTEMPMAX;
static const uint8_t REQUEST_SETTEMPREGULATORMAX = 148;
static const uint8_t REQUEST_SETTEMPAVERAGERATIO = 149;
static const uint8_t REQUEST_SETTEMPAIROFFSET = 150;
static const uint8_t REQUEST_SETTEMPAIRFACTOR = 151;
static const uint8_t REQUEST_SETTEMPREGULATOROFFSET = 152;
static const uint8_t REQUEST_SETTEMPREGULATORFACTOR = 153;
static const uint8_t REQUEST_SETMODULATIONLIMIT25 = 154;
static const uint8_t REQUEST_SETMODULATIONLIMIT50 = 155;
static const uint8_t REQUEST_SETMODULATIONLIMIT75 = 156;
static const uint8_t REQUEST_SETMODULATIONLIMIT100 = 157;
static const uint8_t REQUEST_SETMODULATIONCOMPENSATIONFACTOR = 158;
static const uint8_t REQUEST_SETMINWORKINGPOINT = 159;
static const uint8_t REQUEST_SETMAXWORKINGPOINT = 160;
static const uint8_t REQUEST_SETMODULATIONPEAKREDUCTIONTIME = 162;
static const uint8_t REQUEST_SETBUTTONPRESSEDSTANDBYTIME = 163;
static const uint8_t REQUEST_SETBUTTONPRESSEDMINTIME = 164;
static const uint8_t REQUEST_SETBUTTONPRESSEDMODETIME = 165;
static const uint8_t REQUEST_SETBUTTONPRESSEDMAXTIME = 166; // Deprecated
static const uint8_t REQUEST_SETINDICATORDETECTMODEMAXTIME = 167;
static const uint8_t REQUEST_SETINDICATORDISPLAYMODEMAXTIME = 168;
static const uint8_t REQUEST_SETINDICATORMEASUREFACTOR = 169;
static const uint8_t REQUEST_SETINDICATORPERCENTFACTOR = 170;
static const uint8_t REQUEST_SETPRESTANDBYMAXTIME = 171;
static const uint8_t REQUEST_SETDISCHARGEMINTIME = 172;
static const uint8_t REQUEST_SETAUTOWRITEEEPROMDELAY = 173;
static const uint8_t REQUEST_SETPHASEDETECTIONERRORMAXTIME = 174;
static const uint8_t REQUEST_SETBUTTONPRESSEDRESETTIME = 175;
static const uint8_t REQUEST_SETDISPLAY = 176;
static const uint8_t REQUEST_SETPHASEDETECTIONERRORMAXCOUNT = 177;
static const uint8_t REQUEST_SETDRIVERWORKINGPOINT = 178;
static const uint8_t REQUEST_SETINDICATORDAMPINGFACTOR = 179;
static const uint8_t REQUEST_SETHEATMINTIME = 180;

static const uint8_t REQUEST_SETPARAMSFLAGS = 199;

static const uint8_t REQUEST_GETCONTROLS = 200;
static const uint8_t REQUEST_SETCONTROLSFLAGS = 201;

static const uint8_t REQUEST_SETDRIVERMEASUREAVERAGERATIO = REQUEST_SETMEASUREAVERAGERATIO;
static const uint8_t REQUEST_SETFINALEMEASUREAVERAGERATIO = 210;
static const uint8_t REQUEST_SETDRIVERMINPOINT = REQUEST_SETMINPOINT;
static const uint8_t REQUEST_SETFINALEMINPOINT = 211;
static const uint8_t REQUEST_SETDRIVERMAXPOINT = REQUEST_SETMAXPOINT;
static const uint8_t REQUEST_SETFINALEMAXPOINT = 212;
static const uint8_t REQUEST_SETFINALEWORKINGPOINT = 213;
static const uint8_t REQUEST_SETFINALESTARTINGTRESHOLD = 214;
static const uint8_t REQUEST_SETDRIVERFUNCTIONTRESHOLD = REQUEST_SETFUNCTIONTRESHOLD;
static const uint8_t REQUEST_SETFINALEFUNCTIONTRESHOLD = 215;
static const uint8_t REQUEST_SETDRIVERREGULATIONMAXTIME = REQUEST_SETREGULATIONMAXTIME;
static const uint8_t REQUEST_SETFINALEREGULATIONMAXTIME = 216;
static const uint8_t REQUEST_SETDRIVERREGULATEDMINTIME = REQUEST_SETREGULATEDMINTIME;
static const uint8_t REQUEST_SETFINALEREGULATEDMINTIME = 217;
static const uint8_t REQUEST_SETFINALESTARTP = 218;
static const uint8_t REQUEST_SETFINALESTARTI = 219;
static const uint8_t REQUEST_SETFINALESTARTD = 220;
static const uint8_t REQUEST_SETFINALEREGULATIONP = 221;
static const uint8_t REQUEST_SETFINALEREGULATIONI = 222;
static const uint8_t REQUEST_SETFINALEREGULATIOND = 223;
static const uint8_t REQUEST_SETFINALEFUNCTIONP = 224;
static const uint8_t REQUEST_SETFINALEFUNCTIONI = 225;
static const uint8_t REQUEST_SETDRIVERREGULATIONP = 226;
static const uint8_t REQUEST_SETDRIVERREGULATIONI = 227;
static const uint8_t REQUEST_SETDRIVERFUNCTIONP = 228;
static const uint8_t REQUEST_SETDRIVERFUNCTIONI = 229;
static const uint8_t REQUEST_SETDRIVERREGULATIONTRESHOLD = REQUEST_SETREGULATIONTRESHOLD;
static const uint8_t REQUEST_SETFINALEREGULATIONTRESHOLD = 230;
static const uint8_t REQUEST_SETDRIVERSTARTWORKINGPOINT = REQUEST_SETSTARTWORKINGPOINT;
static const uint8_t REQUEST_SETDRIVERSTARTWORKINGPOINT1 = REQUEST_SETSTARTWORKINGPOINT;
static const uint8_t REQUEST_SETDRIVERSTARTWORKINGPOINT2 = 231;
static const uint8_t REQUEST_SETFINALESTARTWORKINGPOINT = 232;
static const uint8_t REQUEST_SETDRIVEROUTOFRANGEMAXTIME = REQUEST_SETOUTOFRANGEMAXTIME;
static const uint8_t REQUEST_SETFINALEOUTOFRANGEMAXTIME = 233;
static const uint8_t REQUEST_SETDRIVERREGULATIOND = 234;
static const uint8_t REQUEST_SETSTARTD = 235;
static const uint8_t REQUEST_SETREGULATIOND = 236;

// Use REQUEST_PARAMSFLAG_DIFFFEEDBACK instead
// static const uint8_t REQUEST_CONTROLSFLAG_DIFFFEEDBACK = 0x2;

static const uint8_t REQUEST_PARAMSFLAG_WORKINGPOINTAUTO = 0x1;
static const uint8_t REQUEST_PARAMSFLAG_DIFFFEEDBACK = 0x2;
static const uint8_t DEFAULT_PARAMSFLAGS = REQUEST_PARAMSFLAG_WORKINGPOINTAUTO | REQUEST_PARAMSFLAG_DIFFFEEDBACK;

// Base data response
struct DataResponseHeader : SerialResponse
{
    uint8_t step;
    uint8_t stepMaxTime;
    uint8_t stepElapsedTime;
    uint8_t stepMaxValue;
    uint8_t stepCurValue;
    uint8_t tickCount; // 0-60=seconds; > 60=minuts (minus 60); > 120=hours (minus 120)
};
#endif
