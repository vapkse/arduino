#ifndef SerialBuffer_h
#define SerialBuffer_h

// make it a little prettier on the front end.
#define details(name) (byte *)&name, sizeof(name)

#include <stdint.h>
#include "Arduino.h"
#include "Stream.h"
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>

struct SerialRequest
{
    uint8_t id;
    uint8_t msg;
    uint8_t value;
};

struct SerialResponse
{
    uint8_t id;
    uint8_t msg;
    uint8_t errorNumber;
    uint8_t extraValue;
};

class SerialBuffer
{
  public:
    SerialBuffer();
    SerialBuffer(uint8_t *, uint8_t);
    uint8_t *alloc(uint8_t);
    void dispose();
    void begin(Stream *theStream);
    void close();
    void send(Stream *stream);
    void send(Stream *stream, uint8_t);
    boolean receive();
    void reset();
    uint8_t checkSum();
    uint8_t store(uint8_t baseAdress);
    uint8_t restore(uint8_t baseAdress);
    boolean clearStored(uint8_t baseAdress);
    uint8_t getSize();
    void copyTo(void *);
    void copyTo(void *, uint8_t);

  private:
    Stream *_stream;
    uint8_t *address;     // address of struct
    uint8_t max_len;      // size of struct
    uint8_t rx_size;      // RX available size
    uint8_t *rx_buffer;   // address for temporary storage and parsing buffer
    uint8_t rx_array_inx; // index for RX parsing buffer
    uint8_t rx_len;       // RX packet length according to the packet
    uint8_t calc_CS;      // calculated Chacksum
};
#endif
