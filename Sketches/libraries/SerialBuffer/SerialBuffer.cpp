#include "SerialBuffer.h"
#include <EEPROM.h>

SerialBuffer::SerialBuffer() {}

SerialBuffer::SerialBuffer(uint8_t *ptr, uint8_t length)
{
    address = ptr;
    max_len = length;
}

uint8_t *SerialBuffer::alloc(uint8_t length)
{
    address = (uint8_t *)malloc(length + 1);
    max_len = length;
    return address;
}

void SerialBuffer::dispose()
{
    free(address);
    address = NULL;
}

uint8_t SerialBuffer::getSize()
{
    return rx_size > 0 ? rx_size : max_len;
}

// Captures address and size of struct
void SerialBuffer::begin(Stream *theStream)
{
    _stream = theStream;

    // Ensure that serial stream is empty
    while (_stream->available())
    {
        _stream->read();
    }

    // dynamic creation of rx parsing buffer in RAM
    rx_buffer = (uint8_t *)malloc(max_len + 1);
    rx_len = 0;
    rx_array_inx = 0;
}

void SerialBuffer::close()
{
    if (rx_buffer == NULL)
    {
        return;
    }

    free(rx_buffer);
    rx_buffer = NULL;
    rx_len = 0;
    rx_array_inx = 0;
}

// Sends out struct in binary, with header, length info and checksum
void SerialBuffer::send(Stream *stream)
{
    send(stream, getSize());
}

void SerialBuffer::send(Stream *stream, uint8_t length)
{
    uint8_t CS = length;
    stream->write(0x06);
    stream->write(0x85);
    stream->write(length);
    for (int i = 0; i < length; i++)
    {
        CS ^= *(address + i);
        stream->write(*(address + i));
    }
    stream->write(CS);
}

boolean SerialBuffer::receive()
{
    // start off by looking for the header bytes. If they were already found in a previous call, skip it.
    if (rx_len == 0)
    {
        // this size check may be redundant due to the size check below, but for now I'll leave it the way it is.
        if (_stream->available() >= 5)
        {
            // this will block until a 0x06 is found or buffer size becomes less then 3.
            while (_stream->read() != 0x06)
            {
                // This will trash any preamble junk in the serial buffer
                // but we need to make sure there is enough in the buffer to process while we trash the rest
                // if the buffer becomes too empty, we will escape and try again on the next call
                if (_stream->available() < 2)
                    return false;
            }
            if (_stream->read() == 0x85)
            {
                rx_len = _stream->read();
                rx_array_inx = 0;

                // make sure that the allocated memory is enough.
                if (rx_len > max_len)
                {
                    rx_len = 0;
                    return false;
                }
            }
        }
    }

    // we get here if we already found the header bytes, the struct size matched what we know, and now we are byte aligned.
    if (rx_len != 0)
    {
        while (_stream->available() && rx_array_inx <= rx_len)
        {
            rx_buffer[rx_array_inx++] = _stream->read();
        }

        if (rx_len == (rx_array_inx - 1))
        {
            // seem to have got whole message
            // last uint8_t is CS
            calc_CS = rx_len;
            for (int i = 0; i < rx_len; i++)
            {
                calc_CS ^= rx_buffer[i];
            }

            if (calc_CS == rx_buffer[rx_array_inx - 1])
            {
                // CS good
                memcpy(address, rx_buffer, rx_len);
                rx_size = rx_len;
                rx_len = 0;
                rx_array_inx = 0;
                return true;
            }
            else
            {
                // failed checksum, need to clear this out anyway
                rx_size = 0;
                rx_len = 0;
                rx_array_inx = 0;
                return false;
            }
        }
    }

    return false;
}

// Reset the specified buffer
void SerialBuffer::reset()
{
    memset(address, 0, max_len);
}

uint8_t SerialBuffer::checkSum()
{
    uint8_t size = getSize();
    uint8_t cs = size;
    for (uint8_t i = 0; i < size; i++)
    {
        cs ^= *(address + i);
    }
    return cs;
}

// Save the specified structure to the EEPROM at the specified base address
uint8_t SerialBuffer::store(uint8_t baseAdress)
{
    // Calc params check sum
    uint8_t wcs = checkSum();
    EEPROM.write(baseAdress, wcs);
    uint8_t size = getSize();
    for (uint8_t i = 0; i < size; i++)
    {
        EEPROM.write(baseAdress + 1 + i, *(address + i));
    }

    // Verify
    uint8_t rcs = EEPROM.read(baseAdress);
    uint8_t cs = size;
    for (uint8_t i = 0; i < size; i++)
    {
        cs ^= EEPROM.read(baseAdress + 1 + i);
    }

    return cs == rcs ? cs : 0;
}

// Restore the specified structure form the EEPROM
uint8_t SerialBuffer::restore(uint8_t baseAdress)
{
    uint8_t rcs = EEPROM.read(baseAdress);
    if (rcs == 0)
    {
        return 0;
    }
    else
    {
    uint8_t size = getSize();
        uint8_t cs = size;
        for (uint8_t i = 0; i < size; i++)
        {
            *(address + i) = EEPROM.read(baseAdress + 1 + i);
            cs ^= *(address + i);
        }

        return rcs == cs ? cs : 0;
    }
}

// Reset the specified structure
boolean SerialBuffer::clearStored(uint8_t baseAdress)
{
    uint8_t size = getSize();
    for (uint8_t i = 0; i <= size; i++)
    {
        EEPROM.write(baseAdress + i, 0);
    }

    boolean checked = true;
    for (uint8_t i = 0; i <= size; i++)
    {
        if (EEPROM.read(baseAdress + i) != 0)
        {
            checked = false;
            break;
        }
    }

    return checked;
}

void SerialBuffer::copyTo(void *ptr, uint8_t length)
{
    memcpy(ptr, address, length);
}

void SerialBuffer::copyTo(void *ptr)
{
    memcpy(ptr, address, getSize());
}