#ifndef PID_h
#define PID_h
#define LIBRARY_VERSION	1.0.0

class PID
{
public:

	PID(double*, double*, double*, double, double, double,	// * constructor.  Regulator initialization, parameters are: Input, Output, Reference, Working Point, proportional gain, integral gain, derivative gain, 			
		double, double, unsigned long, bool);				//   min output, max output, sample time, reverse

	void SetEnabled(bool value);							// * sets PID mode

	bool Compute();											// * performs the PID calculation.

	void SetOutputLimits(double, double);					// * Define the outputs limits
	
	void SetGains(double, double, double);					// * Set the tunning gain parameters for PID (0 to disable the regulator section)

	void SetSampleTime(unsigned long);						// * sets the frequency, in Milliseconds, with which the PID calculation is performed.

private:
	void Initialize();

	double kP;					// * (P)roportional gain
	double kI;                  // * (I)ntegral gain
	double kD;                  // * (D)erivative gain

	bool reverse;
	bool enabled;

	double *input;              // * Pointers to the Input
	double *output;             // * Pointers to the Output
	double *reference;          // * Pointers to the Reference

	double workingPoint;        // * Pointers to the PID, working point

	double minOutput;
	double maxOutput;

	unsigned long sampleTime;
	unsigned long lastTime;
	double lastInput;
};
#endif

