#include "Blink.h"

Blink::Blink() {
	_pin = -1;
	Reset();
}

void Blink::Setup(uint8_t pin, bool inverted) {	
	_pin = pin;
	_inverted = inverted;
	_state = false;
	_dim = 100;
	pinMode(_pin, OUTPUT); 
	Write();
}

void Blink::Setup(uint8_t pin, bool inverted, uint8_t dim) {
	Setup(pin, inverted);
	_dim = dim;
}

void Blink::Execute(uint16_t ontime, uint16_t offtime)
{
	if (_pin < 0 || ontime <= 0 || offtime <= 0) {
        return;
    }

	if (ontime == _ontime && offtime == _offtime)
	{
	  Execute();
	}
	else
	{
		// Init
		On();
		_ontime = ontime;
		_offtime = offtime;
		_startTime = millis();
	}
}

void Blink::Execute(uint16_t ontime, uint16_t count, uint16_t pauseTime){
    if (_pin < 0 || ontime <= 0  || count <= 0) {
        return;
    }

	if (ontime == _ontime && count == _countMax && pauseTime == _pauseTime)
	{
	  Execute();
	}
	else
	{
		// Init
		Off();
		_ontime = ontime;
		_offtime = ontime;
		_startTime = millis();
		_countMax = count;
		_count = 0;
		_pauseTime = pauseTime;
	}
}

void Blink::Execute() {
    if (_pin < 0 || _ontime <= 0 || _offtime <= 0) {
        return;
    }
	
	if (_countMax > 0 && _count >= _countMax)
	{
		if ((millis() - _startTime) > _pauseTime)
		{
			_count = 0;
			_startTime = millis();
			_state = true;
			Write();
		}
	}
	else if (_state && (millis() - _startTime) > _ontime)
	{
		_startTime = millis();
		_state = false;
		Write();
		if (_countMax > 0)
		{
			_count++;
		}
	}
	else if (_state == LOW && (millis() - _startTime) > _offtime)
	{
		_startTime = millis();
		_state = true;
		Write();
	}
}

void Blink::On() {
	if (_pin < 0) {
        return;
    }

    Reset();
	_state = true;
	Write();
}

void Blink::Off() {
	if (_pin < 0) {
        return;
    }

	Reset();
	_state = false;
	Write();  
}

void Blink::Reset()
{
    _ontime = 0;
    _offtime = 0;
    _startTime = 0;
	_state = false;
	_countMax = 0;
	_count = 0;
	_pauseTime = 0;
}

void Blink::Write()
{
	if (_inverted){
		if (_dim < 100){
			analogWrite(_pin, 255 - (_state ? _dim * 255 / 100 : 0));
		}
		else{
			digitalWrite(_pin, _state ? LOW : HIGH);
		}
	}
	else
	{
		if (_dim < 100){
			analogWrite(_pin, _state ? _dim * 255 / 100 : 0);
		}
		else{
			digitalWrite(_pin, _state ? HIGH : LOW);
		}
	}
}