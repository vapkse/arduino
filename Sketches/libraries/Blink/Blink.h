#ifndef Blink_h
#define Blink_h

#include <Arduino.h>

class Blink {
   public:
    Blink();
    void Setup(uint8_t pin, bool inverted);
    void Setup(uint8_t pin, bool inverted, uint8_t dim);
    void Execute(uint16_t ontime, uint16_t offtime);
    void Execute(uint16_t ontime, uint16_t count, uint16_t pauseTime);
    void Execute();
    void Off();
    void On();
    void On(uint8_t dim);
    void SetDim(uint8_t dim);
    void SetTime(uint32_t now);
    void Reset();

   private:
    void Write();
    bool _inverted;
    uint8_t _pin;
    uint8_t _dim;
    uint16_t _ontime;
    uint16_t _offtime;
    uint16_t _countMax;
    uint16_t _count;
    uint16_t _pauseTime;
    uint32_t _startTime;
    uint8_t _state;
    uint32_t _now;
    uint32_t GetTime();
};
#endif